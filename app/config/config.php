<?php
require '/../../vendor/autoload.php';
$dotenv = new Dotenv\Dotenv('../');
$dotenv->load();


// Set Database parameter

define('DBHOST', getenv('DB_HOST'));
define('DBUSER', getenv('DB_USERNAME'));
define('DBPASS', getenv('DB_PASSWORD'));
define('DBNAME', getenv('DB_DATABASE'));

// APP ROOT
define('APPROOT', getenv('APPROOT'));
define('URLROOT', getenv('URLROOT'));


