<?php
// Include config 

include '../controllers/Users.php';
// var_dump($_POST);
$user = new Users();


if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])){
    $data = $user->register($_POST);
    
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign Up</title>
	<link rel="stylesheet" href="../../../../public/bootstrap/css/bootstrap.min.css">
    <style type="text/css">
        body{ font: 14px sans-serif !important; }
        .wrapper {
        width: 350px;
        padding: 20px;
        margin: 0 auto;
        margin-top: 20px;
        border: 2px solid gray;
}
        .wrapper form {
            margin: 0 auto;
        }
    </style>
</head>
<body>
  
<div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>

        <?php 
            if(isset($data)){
                echo $data;
            }
        ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

            <div class="form-group <?php echo (!empty($data['username_err'])) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control"> 
                <span class="help-block"><?php echo $data['username_err']; ?></span>
            </div>   

            <div class="form-group <?php echo (!empty($data['password_err'])) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                <span class="help-block"><?php echo $data['password_err']; ?></span>
            </div>

            <div class="form-group <?php echo (!empty($data['confirm_password_err'])) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $data['confirm_password']; ?>">
                <span class="help-block"><?php echo $data['confirm_password_err']; ?></span>
            </div>
            <div class="row">
                       <div class="col">
                           <input type="submit" value="Register" name="register" class="btn btn-success btn-block">
                       </div>
                       <div class="col">
                           <a href="login.php" class="btn btn-light btn-block">Have an account? Login</a>
                       </div>
           </div>
            
        </form>
    </div>    

</body>
</html>


 